###############################################################################
#
#Find Encrypted Peptides - This program searches a proteome to detect 
# encrypted antimicrobial peptides.
#Copyright (C) 2021 Marcelo Cardoso dos Reis Melo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


from Bio import SeqIO
import numpy as np
import pandas as pd
import multiprocessing
import os
import time

# Path to your fasta file
databasePath = './'

# File with translated Coding Sequences from the Homo Sapiens genome reconstruction
transCDSfn = "uniprot_sprot_HomoSapiens_WithIsoF_2020_07_17.fasta"

# Name of the output file to be created.
resultsFileName = "newPeptides_8to50_1KperLen.csv"

## Number of threads to use in parallel work.
PROCESSES = 20

## Number of candidates to save per peptide length.
numPepPerLen = 1000

## Minimal and Maximal peptide length
minLen = 8
maxLen = 50

## Parameters for the function defined in Pane, JTB (2017).
## Based on publication:
#
# Title: Antimicrobial potency of cationic antimicrobial peptides can be predicted from 
# their amino acid composition: Application to the detection of “cryptic” antimicrobial peptides.
# DOI: 10.1016/j.jtbi.2017.02.012
##

# Published in Pane
ParM = 0.9
ParN = 1.1

nTer = 1.0
cTer = 0.0

# Hydriphobicity using the Parker-Gly0 scale.

hScale = {"A":0.229,"C":0.274,"D":0,"E":0,"F":0.949,"G":0,"H":0.229,"I":0.873,"K":0,"L":0.949,"M":0.631,
          "N":0,"P":0.229,"Q":0,"R":0.096,"S":0,"T":0.032,"V":0.599,"W":1,"Y":0.484,"X":0}

### Calculates the maximum possible score for a peptide of a given length.
#
# This "ideal" peptide would have Z-many hydorphobic amino acids, and L-Z positively charged residues.
# Iterates of all possible values of "Z", the number of the most hydrophobic residue (with H = 1)
# Assumes all other residues are Arginines (positively charged, with "some" hydrophobicity).
#
def maxScorePaneJTB2017(L, m=0.9, n=1.1):
    return max( [ ((nTer + cTer + L - Z)**m)*(((L - Z)*hScale["R"] + Z)**n) for Z in range(L) ] )

def calcPaneJTB2017(seq, m=0.9, n=1.1, maxScore=1):
    # Charges from N- and C-terminal groups
    charge = nTer + cTer
    # Count Arginines and Lysines
    charge += (seq.count("R") + seq.count("K")) * 1
    # Count Glutamic and Aspartic acids
    charge += (seq.count("E") + seq.count("D")) * -1
    
    # Hydrophobicity
    try:
        H = np.sum( [ hScale[aa] for aa in seq] )
    except:
        H = 0
    
    if charge <= 0:
        score = 0
    else:
        score = (charge**m)*(H**n)/maxScore
    
    return score

## Sanity checks
#testSeq = "PPRGFQPCQAHL"
#maxScore = maxScorePaneJTB2017(len(testSeq))
#print("Maximum score for {}-residue peptides: {}".format(len(testSeq),maxScore))
#print("Test protein score:", calcPaneJTB2017(testSeq , ParM, ParN , maxScorePaneJTB2017(len(testSeq), ParM, ParN) ) )
##

## Open and read FASTA file into a dictionady.
record_dict = SeqIO.to_dict(SeqIO.parse(databasePath + transCDSfn, "fasta"))

print("There are {} protein records in this databse.".format(len(record_dict)))

print("----")

### Parallel process

cols = ["RecID", "start", "len", "ptnLen", "relativeScore", "absoluteScore", "sequence"]
DFlist = []

# Define function to be called in parallel
def procPtn(inputDat):
    
    pep_set = set()

    ParM, ParN, pepLen, maxScore, recid, ptnSeq = inputDat
    
    ptnLen = len(ptnSeq)
    
    resList = []
    
    # Create list of peptides and scores.
    for i in range( ptnLen - pepLen + 1 ):
        pepSeq = str(ptnSeq[i:i+pepLen])
        pep_set.add(pepSeq)
        score = calcPaneJTB2017( pepSeq, ParM, ParN, maxScore)
        resList.append( [recid, i, pepLen, ptnLen, score, score*pepLen, pepSeq ] )
        
    ####################
    # Avoid selecting overlapping candidate peptides.
    ####################

    # We will select subsets of overlaping peptides to pick the one with highest RS.
    startLimPrev = -1
    startLim = np.floor(pepLen/2)

    # Create a DF for all peptides in this ptorein.
    tmpDFptn = pd.DataFrame.from_records(resList, columns=cols)
    # List of selected start positions for this protein.
    startPos = []

    while startLimPrev < (ptnLen - pepLen -1):
        tmpDFpi = tmpDFptn.loc[ tmpDFptn.start <= startLim ].loc[ tmpDFptn.start > startLimPrev ]
        newStart = tmpDFpi.loc[tmpDFpi['relativeScore'].idxmax()]["start"]
        startPos.append( newStart )

        startLimPrev = newStart + np.floor(pepLen/2)
        startLim = startLimPrev + np.floor(pepLen/2)
    
    # Select peptides based on starting potision within protein sequence.
    tmpDFptn = tmpDFptn.loc[ tmpDFptn.start.isin(startPos) ]
    # Exclude relative scores equal to zero
    tmpDFptn = tmpDFptn.loc[ tmpDFptn.relativeScore > 0]
    # Remove repeated sequences (may happen in large proteins)
    tmpDFptn = tmpDFptn.drop_duplicates(subset="sequence", keep="last")
    
    return [tmpDFptn.copy(), pep_set]
    #return tmpDFptn.copy()

# Get terminal width.
terminalWidth = os.get_terminal_size()[0]


startTimeScan = time.time()

# Initialize worker pool
with multiprocessing.Pool(PROCESSES) as procPool:
    pep_set_all = 0
    startTimePepLen = time.time()
    
    # Loop over possible Encrypted AMP length
    for pepLen in range(minLen,(maxLen+1)):
        
        pepStartTime = time.time()
        
        print("Scanning for {}-residue long peptides.".format(pepLen))
        
        # Get maximum possible score of peptides of the current length - for normalization in Absolute Score
        maxScore = maxScorePaneJTB2017(pepLen, ParM, ParN)
        
        ptnsDFs = []
        inputRecs = []
        
        # Loop over all human proteins in the human proteome of proteins in humans.
        for recid,recIter in record_dict.items():
            inputRecs.append([ParM, ParN, pepLen, maxScore, recid, str(recIter.seq)])
        
        ptnCounter = 0
        
        startTime = time.time()
        
        chunksize=50
        # Use iterator-map (from multiprocessing pool) to distribute "N" records at a time to each worker.
        imapIter = procPool.imap(procPtn, inputRecs, chunksize = chunksize)
        
        for res, pep_set_ in imapIter:
        #for res in imapIter:

            ptnsDFs.append( res )
            pep_set_all += len(pep_set_)#pep_set_all.union(pep_set_)
            ptnCounter += 1
            outputStride = (2*PROCESSES*chunksize)
            if not (ptnCounter % outputStride ):
                #print( "We have scanned {} peptides. It took {} seconds.".format(ptnCounter, round(time.time() - startTime, 2)))
                print( "We have scanned {: >5d} peptides in {: 5.1f} minutes. The last {: >5d} peptides took {: 5.1f} seconds.".format(
                            ptnCounter, round((time.time() - pepStartTime)/60,2), outputStride, round(time.time() - startTime, 2) ),
                    end="\r")
                startTime = time.time()
        
        ## Adds a line break so the next "print" happens in the following line.
        print("")
        
        ### Single core test
        #for inputRec in inputRecs:
            #res = procPtn(inputRec)
            #ptnsDFs.append( res )
            
            #ptnCounter += 1
            #if not (ptnCounter % 500):
                #print( "We have scanned {} peptides. It took {} seconds.".format(ptnCounter, round(time.time() - startTime, 2)))
                #startTime = time.time()
        


        # Combine results from all proteins.
        ptnDFLen = pd.concat(ptnsDFs)
        
        # Remove repeated sequences (may happen in protein isoforms)
        ptnDFLen = ptnDFLen.drop_duplicates(subset="sequence", keep="first")
        
        # After all proteins, we sort all elements and keep the top "numPepPerLen"
        ptnDFLen = ptnDFLen.sort_values(by='relativeScore', ascending=False).iloc[0:numPepPerLen]  

        print("{} - We found the highest relative score of {}. Elapsed Time: {}".format( 
            pepLen, 
            round(ptnDFLen["relativeScore"].max(),3),
            round(time.time() - startTimePepLen, 2)
            ) 
        )
        startTimePepLen = time.time()
        
        DFlist.append(ptnDFLen.copy())
            
# After all peptide lengths have been probed, we combine the top results.
allPepScanDF = pd.concat(DFlist)
allPepScanDF.shape

allPepScanDF.to_csv(resultsFileName, index=False)

print("Total scan time was {} seconds.".format(round(time.time() - startTimeScan, 2)))

print ('total num:', pep_set_all)