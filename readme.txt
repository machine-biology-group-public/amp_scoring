﻿The python code in the file “FindEncryptedPeptides.py” is intended to use the method described in Pane, et. al., JTB (2017), DOI: 10.1016/j.jtbi.2017.02.012. Here, we search the entire human proteome in search of the top ranking encrypted peptides. The human proteome was downloaded from UNIProt on July 17th, 2020, and is saved in the file: 
uniprot_sprot_HomoSapiens_WithIsoF_2020_07_17.fasta

The code requires Python >= 3.6, and the packages numpy, pandas, and biopython.
To execute, run the following command in any Linux terminal:

python3 FindEncryptedPeptides.py


The code will search peptides in a user-defined range of peptide lengths. For a given peptide length L, a moving window of length L is applied to every protein in the UNIProt database. To avoid windows with too big overlap, we group windows with “L/2” overlap and choose the one with highest score. The next allowable window starts at the middle of the last accepted window. This avoids windows with more than 50% sequence overlap. Identical sequences are also removed from the final list of windows. Once all allowable windows of size L for all proteins in the proteome are scored and selected, the top 1000 windows with highest score are kept for further analysis.
